# TASK MANAGER 

# DEVELOPER INFO

Name: Anastasia Kharlamova

E-MAIL: aakharlamova@tsconsulting.com

E-MAIL: xarlamovan@inbox.ru

# SOFTWARE

* JDK 11.0.9.1

* Windows OS

# HARDWARE

* RAM 16Gb

* CPU i5

* SSD 500

# BUILD PROGRAMM

```
mvn clean install
```

# RUN PROGRAMM

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

https://yadi.sk/d/TKrZ_3juxQpMUQ?w=1
